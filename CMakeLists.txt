# SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.0)

project(keepassk)

set(QT5_MIN_VERSION 5.12)
set(KF5_MIN_VERSION 5.76)

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH})

include(FeatureSummary)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMSetupVersion)
include(ECMGenerateHeaders)
include(ECMPoQmTools)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

ecm_setup_version(0.1.0
    VARIABLE_PREFIX KEEPASSK
    VERSION_HEADER ${CMAKE_CURRENT_BINARY_DIR}/keepassk-version.h
)


find_package(Qt5 ${QT5_MIN_VERSION} REQUIRED COMPONENTS Core Gui Qml QuickControls2 Svg Widgets)
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS Kirigami2 I18n CoreAddons)

find_package(Corrosion)
set_package_properties(Corrosion PROPERTIES
    PURPOSE "Required for basic functionality"
    DESCRIPTION "CMake scripts to seamlessly build and link to targets using cargo"
    URL https://github.com/AndrewGaspar/corrosion
)

add_subdirectory(src)

install(PROGRAMS org.kde.keepassk.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES org.kde.keepassk.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
