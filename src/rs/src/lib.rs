// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

use keepass::{Database, Result, Error};
use std::fs::File;

pub struct DB {
    database: Database,
}

fn open_database(path: &str, password: &str) -> Box<DB> {
    let db = Database::open(&mut File::open(path).unwrap(), Some(password), None).unwrap();
    Box::new(DB { database: db })
}

fn title_for_index(database: &Box<DB>, index: usize) -> &str {
    let (key, value) = database.database.root.entries.iter().nth(index).unwrap();
    value.get_title().unwrap()
}

fn username_for_index(database: &Box<DB>, index: usize) -> &str {
    let (key, value) = database.database.root.entries.iter().nth(index).unwrap();
    value.get_username().unwrap()
}

fn password_for_index(database: &Box<DB>, index: usize) -> &str {
    let (key, value) = database.database.root.entries.iter().nth(index).unwrap();
    value.get_password().unwrap()
}

fn database_size(database: &Box<DB>) -> usize {
    database.database.root.entries.len()
}

#[cxx::bridge]
mod ffi {
    extern "Rust" {
        type DB;
        fn open_database(path: &str, password: &str) -> Box<DB>;
        fn title_for_index(database: &Box<DB>, index: usize) -> &str;
        fn username_for_index(database: &Box<DB>, index: usize) -> &str;
        fn password_for_index(database: &Box<DB>, index: usize) -> &str;
        fn database_size(database: &Box<DB>) -> usize;
    }
}
