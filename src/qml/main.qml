// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.12 as Kirigami

import org.kde.keepassk 1.0

Kirigami.ApplicationWindow {
    title: "Keepassk"

    globalDrawer: Kirigami.GlobalDrawer {
        isMenu: true
        actions: [
            Kirigami.Action {
                text: i18n("About Keepassk")
                icon.name: "help-about"
                onTriggered: pageStack.layers.push(aboutPage)
                enabled: pageStack.layers.currentItem.title !== i18n("About")
            }
        ]
    }
    pageStack.initialPage: Kirigami.ScrollablePage {
        title: i18n("Database")

        Component.onCompleted: {
            fileDialog.open()
        }
        ListView {
            model: Database {
                id: database
            }
            delegate: Kirigami.SwipeListItem  {
                leftPadding: 0
                rightPadding: 0
                contentItem: Kirigami.BasicListItem {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    text: model.title
                    subtitle: model.username
                }
                actions: [
                    Kirigami.Action {
                        text: i18n("Copy password to Clipboard")
                        icon.name: "password-copy"
                        onTriggered: database.copyPassword(model.password)
                    }
                ]
            }
        }
    }
    FileDialog {
        id: fileDialog
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        nameFilters: [ "KDBX Files (*.kdbx)", "All files (*)" ]
        onAccepted: {
            passwordSheet.path = fileDialog.fileUrl
            passwordSheet.open()
        }
    }
    Kirigami.OverlaySheet {
        id: passwordSheet
        property url path
        header: Kirigami.Heading {
            text: i18n("Password")
        }
        contentItem: TextField {
            id: textField
            echoMode: TextInput.Password
        }
        footer: Button {
            text: i18n("Open")
            onClicked: {
                database.open(passwordSheet.path, textField.text)
                passwordSheet.close()
            }
        }

    }
    Component {
        id: aboutPage
        Kirigami.AboutPage {
            aboutData: database.aboutData
        }
    }
}
