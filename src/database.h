// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QAbstractListModel>
#include <optional>
#include <QUrl>

#include <KAboutData>

#include "lib.rs.h"

class Database : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(KAboutData aboutData READ aboutData CONSTANT)
public:
    enum RoleNames {
        Title = Qt::UserRole + 1,
        Username,
        Password,
    };
    Database(QObject *parent = nullptr);

    Q_INVOKABLE void open(const QUrl &path, const QString &password);
    Q_INVOKABLE void copyPassword(const QString &password);

    int rowCount(const QModelIndex &index) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    KAboutData aboutData() const;
private:
    std::optional<rust::Box<DB>> m_database = std::nullopt;
};
