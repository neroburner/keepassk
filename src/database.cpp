// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "database.h"

#include <QDebug>
#include <QTimer>
#include <QClipboard>
#include <QGuiApplication>
#include <QMimeData>

Database::Database(QObject *parent)
    : QAbstractListModel(parent)
{
}

void Database::open(const QUrl &path, const QString &password)
{
    qDebug() << path;
    beginResetModel();
    m_database = open_database(path.toLocalFile().toStdString(), password.toStdString());
    endResetModel();
}

int Database::rowCount(const QModelIndex &index) const
{
    if(m_database) {
        return database_size(*m_database);
    } else {
        return 0;
    }
}

QVariant Database::data(const QModelIndex &index, int role) const
{
    rust::cxxbridge1::Str string;
    if(role == Title) {
        string = title_for_index(*m_database, index.row());
    } else if(role == Username) {
        string = username_for_index(*m_database, index.row());
    } else if(role == Password) {
        string = password_for_index(*m_database, index.row());

    }
    return QString::fromStdString(std::string(string));
}

QHash<int, QByteArray> Database::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[Title] = "title";
    roleNames[Username] = "username";
    roleNames[Password] = "password";
    return roleNames;
}

void Database::copyPassword(const QString &password)
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    auto mimeData = new QMimeData();
    mimeData->setData(QStringLiteral("x-kde-passwordManagerHint"), QByteArrayLiteral("secret"));
    mimeData->setData(QStringLiteral("text/plain"), password.toLocal8Bit());
    clipboard->setMimeData(mimeData);
    QTimer::singleShot(30000, this, [=](){
        clipboard->clear();
    });
}

KAboutData Database::aboutData() const
{
    return KAboutData::applicationData();
}
